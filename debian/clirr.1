.TH CLIRR "1" "November 2013" "" "User Commands"
.SH NAME
clirr \- Check source and binary compatibility of Java libraries
.SH SYNOPSIS
.B clirr
\fB\-o\fR oldjar \fB\-n\fR newjar
[\fIoptions\fR]

.SH DESCRIPTION

Clirr is a tool that checks Java libraries for binary and source compatibility with older releases. Basically you give it two sets of jar files and Clirr dumps out a list of changes in the public API.

.SH OPTIONS

.HP
\fB\-a\fR, \fB\-\-show\-all\-scopes\fR
.IP
show private and package classes
.HP
\fB\-f\fR, \fB\-\-output\-file\fR <arg>
.IP
output file name
.HP
\fB\-i\fR, \fB\-\-include\-pkg\fR <arg>
.IP
include only classes from this package and its subpackages
.HP
\fB\-n\fR, \fB\-\-new\-version\fR <arg>
.IP
jar files of new version
.HP
\fB\-ncp\fR, \fB\-\-new\-classpath\fR <arg>
.IP
3rd party classpath that is referenced by new\-version
.HP
\fB\-o\fR, \fB\-\-old\-version\fR <arg>
.IP
jar files of old version
.HP
\fB\-ocp\fR, \fB\-\-orig\-classpath\fR <arg>
.IP
3rd party classpath that is referenced by old\-version
.HP
\fB\-p\fR, \fB\-\-show\-pkg\-scope\fR
.IP
show package scope classes
.HP
\fB\-s\fR, \fB\-\-style\fR [text|xml]
.IP
output style

.SH MESSAGES

When clirr generates an ERROR, WARNING or INFO message about a change in the jars being compared, there is an associated message reference code. This manual contains an explanation of the meaning of that message which may contain information which could not be fitted into the brief message summary.

Messages are separated into three severity levels: ERROR, WARNING and INFO.

Errors come in two flavours:

.IP
Link\-time failures, where an exception will be thrown as soon as code compiled against an old version of a class and the new version of the class are loaded into the same classloader hierarchy.
.IP
Run\-time failures, where an exception is thrown when code compiled against the old version of a class attempts to call a method on a new version of the class, or vice versa.

.RS 0

Clirr reports "errors" for cases where it is possible to get a run\-time failure. Whether one actually occurs can depend upon the way the library is called, i.e. changes reported as an error may in fact work when used as long as the patterns of use of the library do not trigger the failure situation.

Warnings are issued for situations where no link or runtime exception will occur, but where the application may behave unexpectedly due to the changes that have occurred.

Information messages provide users with information about new features which have been added without breaking backward compatibility in any way.

When using clirr to report on changes to items which have private or package scope, these changes are always reported as INFO level changes, never WARNING or ERROR level. This allows users of clirr to generate "change reports" at a level suitable for developers without having some of those changes marked (irrelevantly) as binary incompatibilities.

There can never be binary incompatibilities for changes to private classes, methods or fields as that access can only occur from within the same class (i.e. the same compilation unit).

Clirr does not report binary incompatibility WARNINGs or ERRORs for package\-scoped items either, because Java packages are intended to be "release units", i.e. all classes within a package are compiled together (ensuring compatibility) then released as a unit. The only time that package\-scope incompatibilities could possibly be an issue is when users of a library write their own classes using a package declaration belonging to some external library, or when a subset of updated classes (e.g. a single class) from a package is used to override certain classes from a previous release of the library. Both of these situations are considered very poor practice by Java programming convention.

In the following sections, the term "old" is used to refer to a class, interface, method or field from the set of jars which represent the old/previous/original/baseline version of the library being inspected. The term "new" is used to refer to a class, interface, method or field from the set of jars which represent the new/current/latest version of the library being inspected.

In the following sections, the term "type" is used to refer to something which may be either a class or interface.


.TP
.SH 1000 \- Increased visibility of class

Severity: INFO

The specified type exists in both versions, but its declared access specifier has changed to relax restrictions on what other code can access it.

Top\-level types (ie those which are not nested within another class) may only have "package" or "public" accessibility. Nested types can take on any of the four available accessibility values.

Regardless of whether the object is top\-level or nested, a change in accessibility from left\-to\-right of the sequence private\->package\->protected\->public always ensures that all code which could previously access that type can still access that type. Therefore such a change is always binary and source\-code compatible.

Note that the declaration "protected" provides access to both code derived from the type and to code within the same package, ie "protected" accessibility also implies package accessibility.

.TP
.SH 1001 \- Decreased visibility of class

Severity: ERROR

The specified type exists in both versions, but its declared access specifier has changed to tighten the restrictions on what other code may access it.

Top\-level types (ie those which are not nested within another class) may only have "package" or "public" accessibility. Nested types can take on any of the four available accessibility values.

Regardless of whether the type is top\-level or nested, a change in accessibility from left\-to\-right of the sequence public\->protected\->package\->private may cause existing code which could previously access the type to no longer be able to do so.

Section 13.4.3 of the Java Language Specification states explicitly that an IllegalAccessError should occur if a pre\-existing binary tries to access a type when the type accessibility has been changed to something that would cause a compile\-time error. However this does not appear to be enforced in practice, at least in current JVMs. Nevertheless this should be an error, and so clirr reports this change as a binary\-compatibility ERROR. 

.TP
.SH 2000 \- Changed from class to interface

Severity: ERROR

The specified class has become an interface in the new version. This change is always a binary and source\-code incompatibility, for obvious reasons.

.TP
.SH 2001 \- Changed from interface to class

Severity: ERROR

The specified interface has become an class in the new version. This change is always a binary and source\-code incompatibility, for obvious reasons.

.TP
.SH 3001 \- Removed final modifier from class

Severity: INFO

The specified class was declared final in the old version, but is no longer final in the new version.

.TP
.SH 3002 \- Added final modifier to effectively final class

Severity: INFO

The specified class was not declared final in the old version, but is now declared final. Normally, this would be an incompatibility because pre\-existing derived classes would no longer be valid when used with the new version of this class. However in this case the old class version had no public or protected constructors, so it was not possible for any derived classes to exist even for the old version of the library. Changing such a class to final therefore can not break any existing code.

.TP
.SH 3003 \- Added final modifier to class

Severity: ERROR

The specified class was not declared final in the old version, but is now declared final. Any pre\-existing classes which were declared as subclasses of this class will therefore not be valid with the new version of the library.

A VerifyError is thrown by the classloader when an attempt is made to load a subclass of a final class.

Note that a class Y is loaded by the standard classloader only when the first attempt is made to create an instance of Y, or to directly reference the Class object for class Y. If some other class X has class Y as a declared member, or as a parameter to some method, then loading class X does not cause class Y to be loaded.

.TP
.SH 3004 \- Removed abstract modifier from class

Severity: INFO

The old version of this class was declared to be an abstract class. The new version is not abstract, allowing users to create instances of the class.

.TP
.SH 3005 \- Added abstract modifier to class

Severity: ERROR

The old version of this class was not declared to be abstract. The new version is abstract. Pre\-existing code which creates instances of this class is no longer valid with the new version.

.TP
.SH 4000 \- Added interface to the set of implemented interfaces

Severity: INFO

The new version of the type now implements an additional interface. This does not invalidate any existing code (source or binary), and is a completely backward\-compatible change.

Note that this message can be reported without any change occurring in the specified type; a change to the set of interfaces supported by a type will cause this message to be reported for every descendant of that type.

.TP
.SH 4001 \- Removed interface from the set of implemented interfaces

Severity: ERROR

The old version of this type declared that it implemented an interface which the new class or interface does not. Existing code which explicitly or implicitly casts objects of this type to the now missing interface is no longer valid.

Note that this message can be reported without any change occurring in the specified type; a change to the set of interfaces supported by a type will cause this message to be reported for every descendant of that type.

.TP
.SH 5000 \- Added class to the set of superclasses

Severity: INFO or WARNING

The new version of the class has a class in its inheritance hierarchy which the old version did not, either because its direct parent is now a different class, or because one of its parent classes has changed its inheritance hierarchy.

If the specified class has java.lang.Throwable as an ancestor, then this change is reported as a WARNING, because this class change may change the exception\-catching behaviour of programs that use this class.

Note that this message can be reported without any change occurring in the specified class; a change to the set of superclasses of an ancestor class will cause this message to be reported for every descendant class.

.TP
.SH 5001 \- Removed class from the set of superclasses

Severity: ERROR

The old version of this class has a class in its inheritance hierarchy which the new version does not, either because its direct parent is now a different class, or because one of its parent classes has changed its inheritance hierarchy.

Existing code which explicitly or implicitly casts objects of this type to the now missing class type is no longer valid.

Note that this message can be reported without any change occurring in the specified class; a change to the set of superclasses of an ancestor class will cause this message to be reported for every descendent class.

Note also that if this class has Throwable in its ancestry, then the class hierarchy change can also cause changes in the exception\-catching behaviour of programs using this class.

.TP
.SH 6000 \- Added field

Severity: INFO

The new class has an additional static or instance member. This change is completely backwards\-compatible.

.TP
.SH 6001 \- Removed field

Severity: ERROR

The new class has removed a field present in the old version. Pre\-existing code which directly accesses that field will no longer be valid.

.TP
.SH 6002 \- Value of field no longer a compile\-time constant

Severity: WARNING

Code compiled against the old version of the class was permitted to "inline" the value of this field because it was a compile\-time constant. Therefore, existing binary code will continue to use the old value of this field, instead of the new value (which cannot be inlined).

.TP
.SH 6003 \- Value of compile\-time constant has changed

Severity: WARNING

Code compiled against the old version of the class was permitted to "inline" the value of this field because it was a compile\-time constant. Therefore, existing binary code will continue to use the old value of this field, instead of the new value.

.TP
.SH 6004 \- Field type changed

Severity: ERROR

The type associated with the specified static or instance member of the specified class has changed. Pre\-existing code which directly accesses that field may no longer be valid, and therefore this is an incompatible change.

.TP
.SH 6005 \- Field now non\-final

Severity: INFO

The field was previously final, and is no longer final. This means that the field value can now be modified during the lifetime of the class or instance.

Whether a value in a field could previously be "inlined" into other classes is an issue addressed by messages 6002 and 6003, not this message.

.TP
.SH 6006 \- Field now final

Severity: ERROR

The field can no longer be modified during the lifetime of the class or instance. Code which previously modified this field is therefore no longer valid.

.TP
.SH 6007 \- Field now non\-static

Severity: ERROR

The field is now an instance variable rather than a class variable. Code which previously accessed this field via the Class rather than an instance of the class is no longer valid.

.TP
.SH 6008 \- Field now static

Severity: ERROR

The field is now a class variable rather than an instance variable.

For some reason (presumably internal implementation issues), the Java standard declares that this change is not binary\-compatible, and that an IncompatibleClassChangeError will be thrown if code compiled against the "old" version of a class is used together with a "new" version for which a field is now static.

Because source code is permitted to access class variables via instances of that class, this is expected to be a source\-code compatible change. However currently CLIRR reports this as an ERROR for source\-code compatibility too.

.TP
.SH 6009 \- Field More Accessible

Severity: INFO

In the new version, the specified field is accessible to more code than it was previously.

.TP
.SH 6010 \- Field Less Accessible

Severity: ERROR

In the new version, the specified field is accessible to less code than it was previously. Therefore existing code may no longer be valid.

.TP
.SH 6011 \- Removed Constant Field

Binary Severity: WARNING

Source Severity: ERROR

The new class has removed a field present in the old version. Pre\-existing source code which directly accesses that field will no longer be valid.

Previously, however, the field was final and was initialised with a constant value. Therefore code compiled against the previous version of the class will have inlined this constant and will continue to work, using the previous value of this field. A warning is issued as this is often not desirable behaviour. However it is not a binary incompatibility.

.TP
.SH 7000 \- Method now in Superclass

Severity: INFO

The old class had a method named X. The new class no longer has this method, but a parent class does define this method, so no binary or source incompatibility has occurred.

Note that this change may have the effect of forcing the new class to become 'abstract'. If this is the case, then this change is reported separately.

.TP
.SH 7001 \- Method now in Interface

Severity: INFO

The old class or interface previously had a method named X. The new class or interface no longer has this method, but a parent interface does define this method, so no binary or source incompatibility has occurred.

Note that this change may have the effect of forcing the new class to become 'abstract'. If this is the case, then this change is reported separately.

.TP
.SH 7002 \- Method Removed

Severity: ERROR

The old class or interface had a method named X. The new class or interface no longer has this method, and this method is not defined on any parent class or interface.

Whether an error actually occurs at runtime for this change depends on usage patterns. The modified class can be used with existing code as long as that existing code does not attempt to call the removed method. If a call to the missing method is made, then a NoSuchMethodError exception is generated when the method invocation occurs.

.TP
.SH 7003 \- Method Overide Removed

Severity: INFO

The specified method on the old class or interface was overriding an inherited definition. The new class or interface no longer has this method explicitly declared on it, but it still inherits a definition so there is no binary incompatibility.
7004 \- Method Argument Count Changed

Severity: ERROR

The specified method has had arguments added or removed. This means that code which previously invoked it will no longer invoke the same method.

If there is an inherited method definition with the old prototype, then there is no binary incompatibility; code which was compiled against the old version of this class will now invoke the inherited implementation. In this situation, clirr should output an INFO message rather than an error. However at the current date, clirr does not check for this situation.

If there is no inherited method definition with the old prototype, then the change is a binary incompatibility.

.TP
.SH 7005 \- Method Argument Type changed

Binary Severity: INFO or ERROR

Source Severity: ERROR

The specified method has had the type of one or more of its arguments modified. This means that code compiled against the old version of the class will no longer invoke the same method. However exactly the same old source code, when compiled against the new class version may invoke this method if the argument types are assignment\-compatible.

If there is an inherited method definition with the old prototype, then there is no binary incompatibility; code which was compiled against the old version of this class will now invoke the inherited implementation. At the current date, clirr does not check for this situation.

If there is no inherited method definition with the old prototype, then the change is a binary incompatibility.

If the parameter types changed were all changed to supertypes of their previous declared types, or for primitive parameter types if they were changed to "larger" types in every case, then the new code is source\-code\-compatible with the previous release even if it is not binary\-compatible. Note that in this situation, recompiling code which uses the library may change its behaviour from calling an inherited method to calling a method on the class which has a slightly different prototype. At the current date, clirr does not check for this situation.

.TP
.SH 7006 \- Method Return Type changed

Binary Severity: ERROR

Source Severity: INFO or ERROR

The specified method has had its declared return type changed. Whether a problem actually occurs at runtime when using code compiled against the old version of this library depends upon usage patterns. Old code may call other methods on this class. However any attempt to call the method whose return type has changed will result in a NoSuchMethodError being thrown when the method is invoked, because the return type is part of the "method signature".

The change is source\-code\-compatible if and only if the new return type is assignable to the old return type. This means that:

    if the old return type was a primitive type, then the new return type must be narrower than the old type.
    if the old return type was an interface, then the new return type must be a class or interface which implements the old return type.
    if the old return type was a class, then the new return type must be a subclass of the previously returned type.

Clirr does not currently check for source\-code compatibility for changes in method return types; currently these are simply reported as an ERROR.

.TP
.SH 7007 \- Method has been Deprecated

Severity: INFO

The specified method has been declared as "deprecated". This is always a binary\-compatible change as well as a source\-code\-compatible change.

.TP
.SH 7008 \- Method has been Undeprecated

Severity: INFO

The specified method was declared "deprecated" in the previous version, but is no longer deprecated in the current release. While slightly unusual, this is permitted. This change is always a binary\-compatible change as well as a source\-code\-compatible change.

.TP
.SH 7009 \- Method is now Less Accessible

Severity: ERROR

The access permissions associated with the specified method have been tightened to permit less user code to access the method.

Whether this change is a source\-code compatibility issue or not depends upon patterns of usage.

This change should be a binary incompatibility. Note, however, that current JVMs do not validate this. Code compiled against a previous version of a class can successfully invoke methods for which they no longer have access rights. Nevertheless, the Java Language Specification states that this is an error, so clirr reports this change as a binary incompatibility.

.TP
.SH 7010 \- Method is now More Accessible

Severity: INFO

The access permissions associated with the specified method have been loosened to permit more user code to access the method. This is always a binary and source\-code compatible change.

.TP
.SH 7011 \- Method Added

Severity: INFO

A non\-abstract method has been added to the specified class. This is always a binary\-compatible change.

It is also a source\-code compatible change.

Q: if the new method overrides an inherited one, then which version does code compiled against the old library invoke?

.TP
.SH 7012 \- Method Added to Interface

Binary Severity: ERROR

Source Severity: ERROR

A method declaration has been added to the specified interface. This is always reported as a binary\-compatibility error, but in practice the changed class might be used successfully with code compiled against the old interface depending upon usage patterns.

Old code which invokes methods upon code compiled against the new (expanded) interface will continue to work without issues. And old code which implements the old version of the interface will also continue to work correctly as long as no code attempts to invoke any of the newly\-added methods against that instance. But code which (validly) invokes one of the new methods in the interface against an object which implements only the old version of the interface will cause an AbstractMethodError to be thrown at the time the method invocation is attempted.

Adding a method to an interface is always reported as an ERROR, because classes that implement that interface must now be modified to implement the declared method.

.TP
.SH 7013 \- Abstract Method Added to Class

Binary Severity: ERROR

Source Severity: ERROR

An abstract method declaration has been added to the specified class. This is always reported as a binary\-compatibility error, but in practice the changed class might be used successfully with code compiled against the old class depending upon usage patterns.

If instances of objects compiled against the old class are created, then their methods can be invoked without problems. But if the newly\-added abstract method is ever invoked, then an AbstractMethodError is thrown at the time the method invocation is attempted.

.TP
.SH 7014 \- Method now final

Severity: ERROR

The method was previously non\-final, and is now final. Subclasses of this class will no longer compile or run.

When the old class containig this method was final (explicitly or by only providing private constructors) then subclasses cannot exist. Clirr currently does not check for this situation, so this will raise a false alarm in some corner cases.

.TP
.SH 7015 \- Method now non\-final

Severity: INFO

The method was previously final, and is now non\-final. This is always a binary\-compatible change.

.TP
.SH 8000 \- Class Added

Severity: INFO

The new version of the library has a class which was not present in the old version.

.TP
.SH 8001 \- Class Removed

Severity: ERROR

The new version of the library no longer contains the specified class.



.SH EXAMPLES
.HP
Check the compatibility of a library with a previous version:
.IP
clirr \-o foo\-1.0.jar \-n foo\-2.0.jar

.HP
Check the backward compatibility of a new library depending on Apache Commons Logging:
.IP
clirr \-o foo\-1.0.jar \-n foo\-2.0.jar \-ocp /usr/share/java/commons\-logging.jar \-ncp /usr/share/java/commons\-logging.jar


.SH HOMEPAGE

http://clirr.sourceforge.net
